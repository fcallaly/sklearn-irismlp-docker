import json
import requests

model_server = 'http://54.209.177.147:8080/iris/'

# Expecting to interface to/from Lex
def lambda_handler(event, context):
    slots = event['currentIntent']['slots']

    req_iris = {'sepal-length': float(slots['sepal_length']), #6.4,
                'sepal-width': float(slots['sepal_width']), #2.5,
                'petal-length': float(slots['petal_length']), #5.0,
                'petal-width': float(slots['petal_width']) #1.9
                }

    print(req_iris)
    prediction = requests.post(model_server,
    						   json=req_iris)

    print(prediction)

    response = {"dialogAction": {
        "type": "Close",
        "fulfillmentState": "Fulfilled",
        "message": {
            "contentType": "PlainText",
            "content": "Class is: " + prediction.json()["class"]
        }}}

    return response
