FROM python:3.8

COPY ./app app
COPY run.sh run.sh
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

EXPOSE 8080
CMD /bin/sh run.sh

