from flask import Flask, request
from flask_restx import Resource, Api, fields
import joblib

app = Flask(__name__)
api = Api(app)

ns = api.namespace('iris', description='Iris classification')

iris = api.model('iris', {
    'sepal-length': fields.Float(required=True, description='Sepal Length'),
    'sepal-width': fields.Float(required=True, description='Sepal Width'),
    'petal-length': fields.Float(required=True, description='Petal Length'),
    'petal-width': fields.Float(required=True, description='Petal Width')
    })

model = joblib.load('iris_mlp_model.joblib')
label_encoder = joblib.load('iris_mlp_label_encoder.joblib')

@ns.route('/')
class Review(Resource):
    def get(self):
        return {'response': 'iris mlp api is running'}

    @ns.expect(iris)
    def post(self):
        print('payload:')
        print(api.payload)

        prediction = model.predict_proba([[api.payload['sepal-length'],
                                           api.payload['sepal-width'],
                                           api.payload['petal-length'],
                                           api.payload['petal-width']]])[0]
        print('prediction: ' + str(prediction))

        max_prob = max(prediction)
        class_idx = list(prediction).index(max_prob)
        class_str = label_encoder.classes_[class_idx]

        return {'iris': api.payload,
                'class': class_str,
                'probability': max_prob}

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
